$(function () {
  "use strict";
  var currentMode = "none"
  // for better performance - to avoid searching in DOM
  var content = $('#content');
  var input = $('#input');
  var status = $('#status');
  var currentPrompt = $('#currentPrompt');
  var currentOptions = $('#currentOptions');
  var currentUsers = $('#currentUsers');
  var feedElement = $('#guessedFeed');
  var startButton = $('#startGame');
  var clearButton = $('#ClearGuessed');
  let ClearGuessedFunction = () => { feedElement.empty() }
  clearButton.on('click', ClearGuessedFunction);
  let currentGamestate = "notRunning"
  let myResponce:string = ""
  // my name sent to the server
  var myName = "";
  // if user is running mozilla then use it's built-in WebSocket
  window.WebSocket = window.WebSocket
  // if browser doesn't support WebSocket, just show
  // some notification and exit
  if (!window.WebSocket) {
    return;
  }
  // open connection
  //var connection = new WebSocket('ws://10.0.0.69:1337');
  var connection = new WebSocket('ws://206.189.88.59/websocket');
  connection.onopen = function () {
    // first we want users to enter their names
    currentMode = 'setUsername'
    input.removeAttr('disabled');
  };
  connection.onerror = function (error) {
    console.log(error)
    // just in there were some problems with connection...

  };
  // most important part - incoming messages
  connection.onmessage = function (message) {
    // try to parse JSON message. Because we know that the server
    // always returns JSON this should work without any problem but
    // we should make sure that the massage is not chunked or
    // otherwise damaged.
    try {
      var json = JSON.parse(message.data);
    } catch (e) {
      console.log('Invalid JSON: ', message.data);
      return;
    }
    if (json.type === 'setGameState') {  
      currentGamestate = json.data;
      if (currentGamestate == "notRunning") {
        startButton.removeAttr('disabled')
        let startGameFunc = () => {
          connection.send(JSON.stringify({ type: "startGame" }));
        }
        startButton.on('click', startGameFunc);
      }
      else if (currentGamestate == "AwaitingNextRound") {           
        startButton.removeAttr('disabled')
        let startGameFunc = () => connection.send(JSON.stringify({ type: "nextRound" }))
        startButton.on('click', startGameFunc);
      }
      else if (currentGamestate == "GameEnded") {
        startButton.removeAttr('disabled')
        let startGameFunc = () => { connection.send(JSON.stringify({ type: "startGame" })) }
        startButton.on('click', startGameFunc);
        startButton.html('Start Game');
      }
      else {
        startButton.unbind('click')
        startButton.html('Next Round');
        startButton.prop('disabled', true);
      }
    }

    else if (json.type === 'prompt') {
      currentPrompt.empty()
      currentOptions.empty()
      let html = '<p><span> Select a prompt to Answer </span></p>'
      currentOptions.append(html)
      json.data.forEach(prompt => {
        setPromptOption(prompt)
      });
    }
    else if (json.type === 'selectedPrompt') {
      currentPrompt.empty()   
      currentOptions.empty()
      setSelectedPrompt(json.data)
    }
    else if (json.type === 'duplicateMarker') {  
      currentOptions.empty()
      json.data.forEach((prompt,count) => {
        setDuplicateMarking(prompt,count)
      });
      setDuplicateMarkingButtons();
    }
    else if (json.type === 'duplicateViewer') {  
      currentOptions.empty()
      json.data.forEach(prompt => {
        setDuplicateViewing(prompt)
      });
    }
    else if (json.type === 'isWriter') {   
      currentOptions.empty()
      json.data.forEach(prompt => {
        setUnguessedResponcePropt(prompt)
      });
    }    
    else if (json.type === 'allResponces') {  
      currentOptions.empty()
      json.data.forEach(prompt => {
        setResponcePrompt(prompt)
      });
    }
    else if (json.type === 'playerList') {
      currentUsers.empty()  
      json.data.forEach(user => {
        setCurrentUsers(user)
      });
    }
    else if (json.type === 'waitingOnPrompt') {
      currentPrompt.empty()
      currentOptions.empty()
      let html = '<p><span> Waiting on ' + json.data + ' to select a prompt</span></p>'
      currentPrompt.append(html)
    }
    else if (json.type === 'guessedFeed') {
      currentPrompt.empty()
      currentOptions.empty()
      setGuessedFeed(json.data)
    }
    else if (json.type === 'finalScores') {
      currentPrompt.empty()
      currentOptions.empty()
      setFinalScores(json.data)
    }
    else {
      console.log('Unknown JSON', json);
    }
  };
  /**
   * Send message when user presses Enter key
   */
  input.keydown(function (e) {
    if (e.keyCode === 13) {
      var msg = $(this).val();
      if (!msg) {
        return;
      }
      // send the message as an ordinary text
      if(currentMode === 'inputRespone'){
        myResponce = msg.toString();
      }
      let responce = JSON.stringify({ type: currentMode, data: msg })
      connection.send(responce);
      $(this).val('');
      // disable the input field to make the user wait until server
      // sends back response
      input.attr('disabled', 'disabled');
      // we know that the first message sent from a user their name
      if (myName === "") {
        myName = msg.toString();
      }
    }
  });
  /**
   * This method is optional. If the server wasn't able to
   * respond to the in 3 seconds then show some error message 
   * to notify the user that something is wrong.
   */
  setInterval(function () {
    if (connection.readyState !== 1) {
      status.text('Error');
      input.attr('disabled', 'disabled').val(
        'Unable to communicate with the WebSocket server.');
    }
  }, 3000);
  /**
   * Add message to the chat window
   */
  function setPromptOption(prompt: string) {
    let promptCode = prompt.replace(/[^a-zA-Z0-9]/g, '');
    promptCode = promptCode.replace(/[?=]/g, "/");   
    let html = '<p id=' + promptCode + ' class="validOption"><span>' + prompt + '</span></p>'
    currentOptions.append(html)
    var setprompt = $('#' + promptCode);
    let testFunc = () => connection.send(JSON.stringify({ type: "selectedPrompt", data: prompt }))
    setprompt.on('click', testFunc);
  }
  function setSelectedPrompt(prompt: string[]) {   
    let promptCode = prompt[1].replace(/[^a-zA-Z0-9]/g, '');
    promptCode = promptCode.replace(/[?=]/g, "/");  
    let prefix = '<p><span>The prompt for ' + prompt[0] + ' is:</span></p>'
    let html = '<p id=' + promptCode + '><span>' + prompt[1] + '</span></p>'
    currentPrompt.append(prefix)
    currentPrompt.append(html)
    input.removeAttr('disabled');
    currentMode = "inputRespone";
  }
  function setDuplicateMarking(prompt: string,count:number) {
    let promptCode = prompt.replace(/[^a-zA-Z0-9]/g, '');
    promptCode = promptCode.replace(/[?=]/g, "/");  
    let html = '<p id=' + promptCode + ' class="validOption"><span>' + prompt + '</span> <input id = "'+count+'" type="checkbox" class="duplicateCheckbox"/></p>';    
    currentOptions.append(html);   
    let toggleCheckbox = () => {
      var checkbox = $('#'+count);  
      if(checkbox.prop('checked') === true){
        checkbox.prop('checked',false);
      }
      else{
        checkbox.prop('checked',true);
      }
    }
   // var setprompt = $('#' + promptCode);
    //setprompt.on('click', toggleCheckbox);   
  }
  function setDuplicateMarkingButtons(){
    let html = '<button id="markDuplicates">Mark duplicates</button><button id="AllMarked">All duplicates are marked</button>'
    currentOptions.append(html);  
    let sendMarked = () =>{
      let dupeID:string[]=[]     
      var markedElements = $('.duplicateCheckbox');    
      markedElements.each((index,element)=>{  
        //@ts-ignore          
        if(element.checked === true){
          dupeID.push(element.id);
        }
      })
      connection.send(JSON.stringify({ type: "dupeGroup", data: dupeID }))
    }
    let sendAllMarked = ()=>{connection.send(JSON.stringify({ type: "allMarked", data:"" }))}
    $('#markDuplicates').on('click', sendMarked);
    $('#AllMarked').on('click', sendAllMarked);
  }
  function setDuplicateViewing(prompt: string) {
    let promptCode = prompt.replace(/[^a-zA-Z0-9]/g, '');
    promptCode = promptCode.replace(/[?=]/g, "/");  
    let html = '<p id=' + promptCode + ' class="invalidOption"><span>' + prompt + '</span></p>';
    currentOptions.append(html);   
  }
  function setResponcePrompt(prompt: string) {
    let promptCode = prompt.replace(/[^a-zA-Z0-9]/g, '');
    promptCode = promptCode.replace(/[?=]/g, "/");  
    let html = '<p id=' + promptCode + ' class="validOption"><span>' + prompt + '</span></p>';
    currentOptions.append(html);
    var setprompt = $('#' + promptCode);
    let testFunc = () => {
      connection.send(JSON.stringify({ type: "selectedResponcePrompt", data: prompt }))
      currentOptions.empty();
      let selectedHTML = '<p id=' + promptCode + '><span> You selected ' + prompt + '</span></p>';
      currentOptions.append(selectedHTML)
    }
    if(prompt !== myResponce){
    setprompt.on('click', testFunc);   
    }
    else{
      $('#' + promptCode).removeClass("validOption")
      $('#' + promptCode).addClass("invalidOption")
    }
  }
  function setUnguessedResponcePropt(prompt: string) {
    let promptCode = prompt.replace(/[^a-zA-Z0-9]/g, '');
    promptCode = promptCode.replace(/[?=]/g, "/");  
    let html = '<p id=' + promptCode + '><span>' + prompt + '</span></p>';
    currentOptions.append(html);     
  }
  function setCurrentUsers(userName: string) {
    let userCode = userName.replace(/ /g, "_");
    let html = '<p id=' + userCode + '><span>' + userName + '</span></p>'
    currentUsers.append(html)
  }

  function setGuessedFeed(guessedFeed: string[]) {
    guessedFeed.forEach(line => {
      let html = '<p><span>' + line + '</span></p>'
      feedElement.append(html)
      currentOptions.append(html)
    })
  }

  function setFinalScores(finalScores: string[]) {
    finalScores.forEach(line => {
      let html = '<p><span>' + line + '</span></p>'
      currentPrompt.append(html)
    })
  }

});

