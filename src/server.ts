const nsfwDeck = require("./nsfw.json");
let processedNSFWDeck = processCardDeck(nsfwDeck)

export function chatSetup() {
  "use strict";
  // Optional. You will see this name in eg. 'ps' or 'top' command
  process.title = 'node-chat';
  // Port where we'll run the websocket server
  var webSocketsServerPort = 1337;
  // websocket and http servers
  var webSocketServer = require('websocket').server;
  var http = require('http');
  /**
   * Global variables
   */
  // list of currently connected clients (users)
  var clients: Player[] = [];
  var responces = 0
  var selectedResponce = 0
  let currentUserRotation = 0
  var gameState = "notRunning"
  let activeResponces:string[]=[]
  /**
   * Helper function for escaping input strings
   */
  function htmlEntities(str) {
    return String(str)
      .replace(/&/g, '&amp;').replace(/</g, '&lt;')
      .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
  }
  /**
* HTTP server
*/
  var server = http.createServer(function (request, response) {
    // Not important for us. We're writing WebSocket server,
    // not HTTP server
  });
  server.listen(webSocketsServerPort, function () {
    console.log((new Date()) + " Server is listening on port "
      + webSocketsServerPort);
  });
  /**
   * WebSocket server
   */
  var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket
    // request is just an enhanced HTTP request. For more info 
    // http://tools.ietf.org/html/rfc6455#page-6
    httpServer: server
  });
  // This callback function is called every time someone
  // tries to connect to the WebSocket server
  wsServer.on('request', function (request) {
    console.log((new Date()) + ' Connection from origin '
      + request.origin + '.');
    var connection = request.accept(null, request.origin);
    let playerObject: Player = { conn: connection, username: '', enteredResponce: "", guessedResponce: "", currentScore: 0, mainPlayer: false }
    clients.push(playerObject)
    console.log((new Date()) + ' Connection accepted.');
    // user sent some message
    connection.on('message', function (message) {
      if (message.type === 'utf8') {
        try {
          var decodedMessage = JSON.parse(message.utf8Data);
        } catch (e) {
          console.log('Invalid JSON: ', message.utf8Data);
          return;
        }
        if (decodedMessage.type == "setUsername") {
          let userNameList: string[] = []
          clients.forEach(client => {
            if (client.conn == connection) {
              client.username = decodedMessage.data
              client.conn.sendUTF(JSON.stringify({ type: 'setGameState', data: gameState }));
            }
            userNameList.push(client.username)
          })
          clients.forEach(client => {
            client.conn.sendUTF(JSON.stringify({ type: 'playerList', data: userNameList }))
          })
        }
        else if (decodedMessage.type == "startGame") { 
          if(gameState === "notRunning"){ 
            gameState = "SelectingPrompt";   
          let propmtOptions:string[]=[]
          for(let i=0;i<3;i++){
            propmtOptions.push(drawCard(processedNSFWDeck).cardText.toLowerCase())
          }    
          console.log(propmtOptions)      
          clients.sort((a, b) => { return Math.random() - 0.5; });
          clients.forEach(client => { client.currentScore = 0 });         
          sendGameState(clients, gameState);
          activeResponces = [];
          startRound(clients, currentUserRotation, propmtOptions);
        }
        }
        else if (decodedMessage.type == "selectedPrompt") {
          let userNameData: string[] = [clients[currentUserRotation].username, decodedMessage.data];
          clients.forEach(client => {
            client.conn.sendUTF(
              JSON.stringify({ type: 'selectedPrompt', data: userNameData }));
          })
          gameState = "AwaitingAnswers";
          sendGameState(clients, gameState)
        }
        else if (decodedMessage.type == "inputRespone") {
          clients.forEach(client => {
            if (client.conn == connection) {
              client.enteredResponce = decodedMessage.data
            }
          })
          responces = responces + 1
          if (responces === clients.length) {           
            clients.forEach(client => {
              activeResponces.push(client.enteredResponce)
            });
            activeResponces.sort((a, b) => { return Math.random() - 0.5; });
            clients.forEach((client, count) => {
              if (count === currentUserRotation) {
                client.conn.sendUTF(JSON.stringify({ type: "duplicateMarker", data: activeResponces }));
                client.guessedResponce = undefined;
              }
              else {
                client.conn.sendUTF(JSON.stringify({ type: "duplicateViewer", data: activeResponces }));
              }
            })
            responces = 0
            gameState = "markingDuplicates";
            sendGameState(clients, gameState)
          }    
        }
        else if (decodedMessage.type == "dupeGroup") {
          let dupeIDs:string[] = decodedMessage.data
          let dupeIDNum:number[]=[]
          let truthFlagged = false
          dupeIDs.forEach(id=>{
            dupeIDNum.push(parseInt(id));
          })
          dupeIDNum.sort((a,b)=>{return b-a})
          let correctAns = clients[currentUserRotation].enteredResponce
          dupeIDNum.forEach(idnum=>{
            if(activeResponces[idnum] === correctAns){
              truthFlagged = true;
            }
          })
          //@ts-ignore
          if(truthFlagged === true){
            let guessedFeed:string[]=[]
            dupeIDNum.forEach(idNum=>{
              let dupe = activeResponces[idNum]
              clients.forEach((client,count)=>{
                if(client.enteredResponce === dupe){
                  if(count === currentUserRotation){
                    guessedFeed.unshift(client.username + " entered " + dupe + " as the correct answer")
                  }
                  else{
                    client.currentScore +=4;
                    guessedFeed.push(client.username + " wrote " + dupe)
                  }
                }
              })              
        
            })
            selectedResponce = 0
            clients.forEach(client => {
              client.conn.sendUTF(JSON.stringify({ type: "guessedFeed", data: guessedFeed }))
            })     
            if (checkGameEnd(clients)) {
              gameState = "GameEnded";
              clients.sort((a, b) => { return a.currentScore - b.currentScore });
              let clientFinalScores: string[] = [];
              clients.forEach(client => {
                clientFinalScores.push(client.username)
              });
              sendGameState(clients, gameState);
              clients.forEach(client => {
                client.conn.sendUTF(JSON.stringify({ type: "finalScores", data: clientFinalScores }));
              })
            }
            else {
              gameState = "AwaitingNextRound";
              sendGameState(clients, gameState)
            }
          }
          else{
            dupeIDNum.forEach(idnum=>{
              activeResponces.splice(idnum,1)
            })
            clients.forEach((client, count) => {
              if (count === currentUserRotation) {
                client.conn.sendUTF(JSON.stringify({ type: "duplicateMarker", data: activeResponces }));
                client.guessedResponce = undefined;
              }
              else {
                client.conn.sendUTF(JSON.stringify({ type: "duplicateViewer", data: activeResponces }));
              }
            })
          }
        }        
        else if (decodedMessage.type == "allMarked") {        
          clients.forEach((client, count) => {
            if (count === currentUserRotation) {
              client.conn.sendUTF(JSON.stringify({ type: "isWriter", data: activeResponces }));
              client.guessedResponce = undefined;
            }
            else {
              client.conn.sendUTF(JSON.stringify({ type: "allResponces", data: activeResponces }));
            }
          })
          responces = 0
          gameState = "AwaitingGuesses";
          sendGameState(clients, gameState)
        }

        else if (decodedMessage.type == "selectedResponcePrompt") {
          clients.forEach(client => {
            if (client.conn == connection) {
              client.guessedResponce = decodedMessage.data
            }
          })          
          selectedResponce = selectedResponce + 1
          if (selectedResponce === clients.length - 1) {            
            let responceMap = responcesToMap(clients);
            let guessedFeed: string[] = [];
            let correctResponce = clients[currentUserRotation].enteredResponce;
            guessedFeed.push("The correct answer is " + correctResponce);
            clients.forEach((client, count) => {
              if (client.guessedResponce !== undefined) {
                let otherPlayer = responceMap.get(client.guessedResponce)
                if (otherPlayer.mainPlayer === true) {
                  client.currentScore += 2;
                  otherPlayer.currentScore += 1;
                  guessedFeed.push(client.username + ' guessed correctly')
                }
                else if (otherPlayer) {
                  otherPlayer.currentScore += 1;
                  guessedFeed.push(client.username + " guessed " + otherPlayer.enteredResponce + " (" + otherPlayer.username + ")")
                }
                else {
                  console.log(client.username + "didn't have a valid answer")
                }
              }
            })
            selectedResponce = 0
            clients.forEach(client => {
              client.conn.sendUTF(JSON.stringify({ type: "guessedFeed", data: guessedFeed }))
            })
            if (checkGameEnd(clients)) {
              gameState = "GameEnded";
              clients.sort((a, b) => { return a.currentScore - b.currentScore });
              let clientFinalScores: string[] = [];
              clients.forEach(client => {
                clientFinalScores.push(client.username)
              });
              sendGameState(clients, gameState);
              clients.forEach(client => {
                client.conn.sendUTF(JSON.stringify({ type: "finalScores", data: clientFinalScores }));
              })
            }
            else {
              gameState = "AwaitingNextRound";
              sendGameState(clients, gameState)
            }
          }
        }
        else if (decodedMessage.type == "nextRound") { 
          if(gameState == "AwaitingNextRound")  {     
           gameState = "SelectingPrompt";
          currentUserRotation += 1;
         
          if (currentUserRotation === clients.length) {
            currentUserRotation = 0;
          }
          activeResponces = [];
          let propmtOptions:string[]=[]
          for(let i=0;i<3;i++){
            propmtOptions.push(drawCard(processedNSFWDeck).cardText.toLowerCase())
          }  
          console.log(propmtOptions)
          startRound(clients, currentUserRotation, propmtOptions)
          
          sendGameState(clients, gameState)
        }
        }
      }
    });
    // user disconnected
    connection.on('close', function (connection) {
      let index: number | undefined = undefined
      clients.forEach((client, count) => {
        if (client.conn.state === "closed") {
          index = count
        }
      })
      if (index) {
        clients.splice(index, 1)
      }
      let userNameList: string[] = []
      clients.forEach(client => {
        userNameList.push(client.username);
      })
      clients.forEach(client => {
        client.conn.sendUTF(JSON.stringify({ type: 'playerList', data: userNameList }))
      })
    });
  });
}

type Player = { conn: any, username: string, currentScore: number, enteredResponce: string, guessedResponce: string, mainPlayer: boolean }

function responcesToMap(clients: Player[]) {
  let responceMap: Map<string, Player> = new Map()
  clients.forEach(client => {
    responceMap.set(client.enteredResponce, client);
  })
  return responceMap
}

function startRound(clients: Player[], currentUserRotation: number, prompt: string[]) {
  clients.forEach((client, count) => {
    if (count === currentUserRotation) {
      client.mainPlayer = true;
      client.conn.sendUTF(
        JSON.stringify({ type: 'prompt', data: prompt }));
    }
    else {
      client.conn.sendUTF(
        JSON.stringify({ type: 'waitingOnPrompt', data: clients[currentUserRotation].username }));
    }
  })
}

function sendGameState(clients: Player[], gameState: string) {
  clients.forEach(client => {
    client.conn.sendUTF(
      JSON.stringify({ type: 'setGameState', data: gameState }));
  })

}

function checkGameEnd(clients: Player[]) {
  let finalValue = false;
  clients.forEach(client => {
    if (client.currentScore > 25) {
      finalValue = true;
    }
  })
  return finalValue;
}
type Card = {cardText:string,cardtags:string[]};
function processCardDeck(json:any){
  let cardDeck:Card[]=[];
  json.carddeck.forEach(card => {
    let nextCard:Card = {cardText:card.cardText, cardtags:[]};
    cardDeck.push(nextCard);
  });
 return cardDeck;
} 

function drawCard(cardDeck:Card[]):Card{
  let cardNumber = getRandomInt(cardDeck.length);
  let drawnCard = cardDeck.splice(cardNumber,1)
  return drawnCard[0]
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}